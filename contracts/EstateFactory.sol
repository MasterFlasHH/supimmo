pragma solidity ^0.7.4;

import "./Ownable.sol";
import "./SafeMath.sol";

contract EstateFactory is Ownable {

  using SafeMath for uint256;

  struct Estate {
    uint256 id;
    string name;
    string physicalAddress;
    uint squaredMeterSize;
    uint estateType;
    uint price;
    string image;
  }

  enum EstateType {
    HOUSE,
    FLAT,
    GARAGE,
    FIELD
  }

  mapping (address => uint) public ownerEstateCount;
  mapping (uint => address) public estateToOwner;

  Estate[] public estates;

  modifier onlyOwnerOf(uint _tokenId) {
    require(msg.sender == estateToOwner[_tokenId]);
    _;
  }

  function _createEstate(Estate memory estate) internal {
    estates.push(estate);
    uint id = estates.length.sub(1);
    estates[id].id = id;
    estateToOwner[id] = msg.sender;
    ownerEstateCount[msg.sender] = ownerEstateCount[msg.sender].add(1);
  }

  function createEstate(string memory name, string memory physicalAddress, uint squaredMeterSize, uint estateType, uint price, string memory image) public {
    require(price >= 10, "Le prix du bien est trop faible (10 wei minimum)");
    Estate memory estate = Estate(0, name, physicalAddress, squaredMeterSize, estateType, price, image);
    _createEstate(estate);
  }
}