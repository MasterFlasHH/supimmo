pragma experimental ABIEncoderV2;

import "./ERC721.sol";
import "./EstateFactory.sol";
import "./SafeMath.sol";

/**
 * @title EstateOwnership
 * @dev Contract implementing ERC721 standard
 */
contract EstateOwnership is ERC721, EstateFactory {

  using SafeMath for uint256;

  mapping (uint => address) estateApprovals;

  function balanceOf(address _owner) override public view returns (uint256 _balance) {
    return _owner.balance;
  }

  function ownerOf(uint256 _tokenId) override public view returns (address _owner) {
    return estateToOwner[_tokenId];
  }

  function _transfer(address _from, address _to, uint256 _tokenId) private {
    ownerEstateCount[_to] = ownerEstateCount[_to].add(1);
    ownerEstateCount[msg.sender] = ownerEstateCount[msg.sender].sub(1);
    estateToOwner[_tokenId] = _to;
    Transfer(_from, _to, _tokenId);
  }   

  function _takeOwnership(uint256 _tokenId) private {
    address owner = ownerOf(_tokenId);
    _transfer(owner, msg.sender, _tokenId);
  }

  function transfer(address _to, uint256 _tokenId) override public onlyOwnerOf(_tokenId)  {
    _transfer(msg.sender, _to, _tokenId);
  }

  function approve(address _to, uint256 _tokenId) override public onlyOwnerOf(_tokenId)  {
    estateApprovals[_tokenId] = _to;
    Approval(msg.sender, _to, _tokenId);
  }

  function takeOwnership(uint256 _tokenId, uint amount) override payable public {
    require(estateApprovals[_tokenId] == msg.sender, "Vous n'avez pas l'autorisation");
    require(msg.value == amount, "Le montant n'est pas correct");
    require(msg.value == estates[_tokenId].price, "Le montant de la transaction n'est pas correct");
    payable(estateToOwner[_tokenId]).transfer(amount * 90 / 100);
    payable(address(this)).transfer(amount * 10 / 100);

    _takeOwnership(_tokenId);
  }

  function getAllEstates() public view returns(Estate[] memory test) {
    return estates;
  }
}