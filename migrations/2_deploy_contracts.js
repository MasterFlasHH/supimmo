const EstateFactory = artifacts.require("EstateFactory");
const EstateOwnership = artifacts.require("EstateOwnership");
const Ownable = artifacts.require("Ownable");
const SafeMath = artifacts.require("SafeMath");

module.exports = function(deployer) {
  deployer.deploy(EstateFactory);
  deployer.deploy(EstateOwnership);
  deployer.deploy(Ownable);
  deployer.deploy(SafeMath);
};
