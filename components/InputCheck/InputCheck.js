import React, {forwardRef, useImperativeHandle, useState} from 'react';
import './style.less';
import {notification} from 'antd';
import {ExclamationOutlined} from '@ant-design/icons';

const InputCheck = forwardRef((props, ref) => {
  const [classNameInput, setClassNameInput] = useState(null);
  const [errorInputMsg, setErrorInputMsg] = useState(null);
  const [errorInputResult, setErrorInputResult] = useState(false);

  useImperativeHandle(ref, () => ({
    actionInput: (action) => {
      if (action === 'checkRules') {
        return checkRules();
      } else if (action === 'resetInputValidity') {
        setErrorInput();
      }
    }
  }));

  function onFocus() {
    setErrorInput();
  }

  function onBlur() {
    checkRules();
  }

  function setErrorInput(error = false, msg = '') {
    if (error === true) {
      setClassNameInput('errorInput');
      setErrorInputResult(true);
      setErrorInputMsg(msg);
    } else {
      setClassNameInput(null);
      setErrorInputResult(false);
      setErrorInputMsg(null);
    }
  }

  function checkRules() {
    let error = { value: false, msg: '' };
    const rules = props.rules;
    if (rules) {
      let inputValue = props.children.props.value;
      if (typeof inputValue === 'string') {
        inputValue = inputValue.trim();
      }
      if (rules.required && rules.required === true && !inputValue) {
        error = { value: true, msg: 'Le champ est obligatoire !' };
      } else if (rules.maxLength && inputValue.length > parseInt(rules.maxLength)) {
        error = {
          value: true,
          msg: 'Le nombre de caractères doit être inférieur à ' + parseInt(rules.maxLength + 1) + ' !'
        };
      } else if (rules.minLength && inputValue.length < parseInt(rules.minLength)) {
        error = {
          value: true,
          msg: 'Le nombre de caractère doit être supérieur à ' + parseInt(rules.minLength - 1) + ' !'
        };
      }
      setErrorInput(error.value, error.msg);
    }

    return error.value;
  }

  function suffixLabelRequired() {
    let suffix = '';
    if (props.rules && props.rules.required && props.rules.required === true) {
      suffix = '*';
    }
    return suffix;
  }

  return (
    <div className="mainContainerInputCheck">
      <div hidden={!props.label} style={{fontSize: 11}} className={errorInputResult ? 'errorColor' : ''}>
        {props.label + ' ' + suffixLabelRequired()}
      </div>
      <div className={classNameInput} onFocus={onFocus} onBlur={onBlur}>
        {props.children}
      </div>
      <div hidden={!errorInputResult} className="errorMsgInput">{errorInputMsg}</div>
      <div hidden={!errorInputResult} className="errorIconInput"><ExclamationOutlined /></div>
    </div>
  );
});

export function resetInputValidity(inputs = []) {
  Object.keys(inputs).forEach((key) => {
    inputs[key].actionInput('resetInputValidity');
  });
}

export function checkFormValidity(inputs = []) {
  let formValid = true;
  Object.keys(inputs).forEach((key) => {
    let error = inputs[key].actionInput('checkRules');
    if (error === true && formValid === true) {
      formValid = false;
      notification['error']({
        message: 'Erreur formulaire',
        description: 'La validation du formulaire est impossible car celui-ci contient des erreurs'
      });
    }
  });

  return formValid;
}

export default InputCheck;