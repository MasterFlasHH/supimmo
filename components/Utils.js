import {Button, notification, Modal, Tooltip} from 'antd';
import {QuestionCircleOutlined, FileExcelOutlined} from '@ant-design/icons';
import React from 'react';

/**
 * Fonction permettant de générer une tooltip d'aide pour les inputs
 *
 * @param message
 * @param placement
 */
export function helpTooltipInput(message, placement = 'topRight') {
  return (
    <Tooltip placement={placement} title={message}>
      <QuestionCircleOutlined />
    </Tooltip>
  );
}

/**
 * Fonction permettant d'afficher une notification avec une icone
 *
 * @param type
 * @param title
 * @param message
 * @param duration
 */
export function openNotificationWithIcon(type, title, message, duration = 5) {
  notification[type]({
    message: title,
    description: message,
    duration: duration
  });
}

/**
 * Fonction permettant d'afficher un bouton d'export CSV avec une redirection vers le lien d'export au clique
 *
 * @param urlExport
 * @returns {*}
 */
export function btnExportCSV(urlExport) {
  return <Button icon={<FileExcelOutlined />} type="primary"
                 size="large" onClick={() => { window.open(urlExport); }}>
    Export CSV
  </Button>;
}

/**
 * Fonction permettant d'afficher un message d'erreur de requête axios en fonction de l'erreur (Server ou Client)
 *
 * @param type
 * @param title
 * @param message
 * @param error
 * @param duration
 */
export function openNotificationAxiosRequestError(type, title, message, error, duration = 5) {
  let msgErreur = '';
  if (error && error.response && error.response.data && error.response.data.message) {
    error = error.response.data.message;
  }

  if (error) {
    msgErreur = '(Erreur: ' + error + ')';
  }

  notification[type]({
    message: title,
    description: message + ' ' + msgErreur,
    duration: duration
  });
}

/**
 * Fonction permettant de mettre en évidence un texte recherche dans une chaine de caractère
 *
 * @param searchtext
 * @param text
 * @returns {*}
 */
export function highlightSearchtextInStr(searchtext, text) {
  let elementFinal = [];
  let elementHighlight = [];
  text.replace(new RegExp(searchtext, 'gi'), (searchtextHighlight) => {
    elementHighlight.push(<span className="highlight">{searchtextHighlight}</span>);
  });
  text.split(new RegExp(searchtext, 'gi')).map((fragment, i) => {
    elementFinal.push(fragment);
    if (elementHighlight[i]) {
      elementFinal.push(React.cloneElement(elementHighlight[i], { key: i }));
    }

    return null;
  });

  return <span>{elementFinal}</span>;
}

/**
 * Fonction permettant de formater une info si elle est vide
 *
 * @param info
 * @returns {*}
 */
export function formatInfoIfEmpty (info) {
  let parseInfo = info;
  if (!parseInfo) {
    parseInfo = <span className="emptyInfo">Non renseigné</span>;
  }

  return parseInfo;
}

/**
 * Fonction permettant de copier une chaîne de caractère dans le press-papier
 * @param str
 * @param uppercase
 */
export function copyToClipboardStr(str, uppercase = true) {
  if (uppercase) {
    str = str.toUpperCase();
  }
  const tmpInput = document.createElement('input');
  tmpInput.value = str;
  document.body.appendChild(tmpInput);
  tmpInput.select();
  document.execCommand('copy');
  document.body.removeChild(tmpInput);
}

/**
 * Fonction permettant de mettre la première lettre d'une chaine de caractère en majuscule
 * @param string
 * @returns {string}
 */
export function firstLetterUppercase(string) {
  if (typeof string === 'string') {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }
}

/**
 * Fonction permettant de retourner une image si une source existe sinon une image not found
 * @param img
 * @returns {*}
 */
export function imgWithNotFound(img) {
  if (!img) {
    img = '/img/img_not_found.jpg';
  }

  return <img src={img} alt="Logo"/>;
}

/**
 * Fonction permettant de convertir un booleen en "Oui" "Non"
 *
 * @param boolean
 * @returns {string}
 */
export function booleanToString(boolean) {
  let str = 'Non';
  if (boolean === true) {
    str = 'Oui';
  }

  return str;
}