import React from 'react';
import HeadCustom from '../HeadCustom/HeadCustom';
import dynamic from 'next/dynamic';
import {motion} from 'framer-motion';
import './style.less';

const MatomoReact = dynamic(
  import('../MatomoReact'),
  { ssr: false }
);


function LayoutCustom(props) {
  const variants = {
    visible: { opacity: 1 },
    hidden: { opacity: 0 }
  };


  return (
    <main>
      <HeadCustom title={props.title} description={props.description}/>
      <MatomoReact/>
      <motion.div initial="hidden" animate="visible" exit="hidden" variants={variants} transition={{ duration: 0.4 }}>
        <div className="mainContainerContent">{props.children}</div>
      </motion.div>
    </main>
  );
}

export default LayoutCustom;