import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {openNotificationAxiosRequestError} from '../Utils';
import {Select} from 'antd';

const Option = Select.Option;

function SelectWithData(props) {
  const [data, setData] = useState(props.data || []);
  const [dataSave, setDataSave] = useState(props.data || []);

  useEffect(() => {
    if (props.urlData) {
      getData();
    }
  }, []);

  function getData() {
    axios.get(props.urlData).then((response) => {
      setData(response.data);
      setDataSave(response.data);
    }).catch((error) => {
      openNotificationAxiosRequestError('error', 'Récupération des données', 'la récupération des données a échoué', error);
    });
  }

  function generateOptions(data) {
    if (data.length === 0 && !props.urlData) {
      data = props.data;
    }
    let options = [];
    if (data) {
      data.forEach((element) => {
        if (options.length < 300 || props.value === element[props.idField]) {
          options.push(<Option value={element[props.idField]}
                               key={element[props.idField]}>{element[props.libField]}</Option>);
        }
      })
    }

    return options;
  }

  function handleChange(value) {
    props.onSelectChange(value);
  }

  function onDropdownVisibleChange(value) {
    if (data.length === 0 && !props.urlData) {
      setData(props.data);
      setDataSave(props.data);
    }

    if (value === true) {
      setData(dataSave.slice(0, 299));
    }
  }

  function onSearch(value) {
    let optionsData = [];
    dataSave.forEach((element) => {
      if (element[props.libField] && element[props.libField].toLowerCase().indexOf(value.toLowerCase()) >= 0) {
        optionsData.push(element);
      }
    });

    setData(optionsData);
  }

  return (
    <Select mode={props.mode} showSearch style={{ width: props.widthSelect }} onSearch={onSearch}
            onDropdownVisibleChange={onDropdownVisibleChange} disabled={props.disabled} size={props.size}
            value={props.value || []} placeholder={props.placeholderLib}
            optionFilterProp="children" onChange={handleChange}>
      {generateOptions(data)}
    </Select>
  );
}

export default SelectWithData;