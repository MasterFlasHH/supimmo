import axios from 'axios';
import {apiUrl} from '../../config';
import {openNotificationAxiosRequestError} from '../Utils';

export function getUserByMatricule(matricule) {
  return axios.get(apiUrl + '/referential/users/' + matricule).then((response) => {
    return response.data
  }).catch((error) => {
    openNotificationAxiosRequestError('error', 'Récupération des données', 'la récupération des infos de l\'utilisateur ' + matricule + ' a échoué', error);
    throw error;
  });
}