import React from 'react';
import Nexthead from 'next/head'

function HeadCustom(props) {
  return (
    <Nexthead>
      <title>{props.title || 'NextJS App Model'}</title>
      <meta charSet="utf-8"/>
      <meta name="description" content={props.description || 'Ceci un model de projet NextJS'}/>
      <meta name="author" content="Dylan COMPERE"/>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <meta name="theme-color" content="#59285c"/>
      <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
      <link rel="manifest" href="/manifest.json"/>
      <link rel="shortcut icon" href="/favicon.ico"/>
      <link rel="apple-touch-icon" href="/apple-touch-icon.png"/>
    </Nexthead>
  );
}

export default HeadCustom;