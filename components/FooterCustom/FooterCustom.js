import React from 'react';
import './style.less';

function FooterCustom(props) {
  function getYear() {
    const date = new Date();
    return date.getFullYear();
  }

  return (
    <div className="mainContainerFooter">
      SupImmo ©{getYear()}</div>
  );
}

export default FooterCustom;