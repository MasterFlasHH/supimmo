import React from 'react';
import {Menu} from 'antd';
import Link from 'next/link';
import {useRouter} from 'next/router';

function MenuCustom(props) {
  const router = useRouter();

  function getSelectedKey() {
    const pathname = router.pathname;
    return [pathname.substr(1)];
  }

  return (
    <Menu theme="light" selectedKeys={getSelectedKey()}>
      <Menu.Item key="manage-real-estate">
        <Link href="/manage-real-estate">
          <span className="nav-text">Annonces immobilières</span>
        </Link>
      </Menu.Item>
    </Menu>
  );
}

export default MenuCustom;