import * as jwt from 'jsonwebtoken';
import axios from 'axios';
import {apiUrl, secretKey, storageKey} from '../../config';
import {openNotificationAxiosRequestError} from '../Utils';
import {useEffect, useState} from 'react';

export function Auth() {

  const removeAddress = () => {
    localStorage.removeItem(storageKey);
  };

  const saveAddress = (token) => {

    localStorage.setItem(storageKey, token)
  };

  const getAddress = () => {
    return localStorage.getItem(storageKey);
  };

  const verifyAddress = () => {
    const token = getAddress();
    let decodedToken = null;
    try {
      decodedToken = jwt.verify(token, secretKey, { algorithms: ['HS256'] });
    } catch (e) {
      if (token) {
        removeAddress();
      }
    }

    return decodedToken;
  };

  const disconnect = (router) => {
    removeAddress();
    router.push({
      pathname: '/login'
    });
  };

  return {removeAddress, saveAddress, getAddress, verifyAddress, disconnect }
}

/**
 * Fonction permettant de vérifier l'accès aux routes
 * @param router
 * @param children
 * @returns {null}
 * @constructor
 */
export function RouterAccessControl({ router, children }) {
  const [view, setView] = useState(null);
  useEffect(() => {
    if (router.pathname === '/login' &&  router.pathname === '' &&  router.pathname === '/' && Auth().verifyAddress()) {
      router.push({
        pathname: '/manage-real-estate'
      })
    } else if (router.pathname !== '/login' && !Auth().verifyAddress()) {
      router.push({
        pathname: '/login'
      });
    } else {
      setView(children)
    }
  }, [router.pathname]);

  return view;
}