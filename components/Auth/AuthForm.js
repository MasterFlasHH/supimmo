import React, {useEffect, useState} from 'react';
import * as jwt from 'jsonwebtoken';
import './style.less';
import {Button} from 'antd';
import {useRouter} from 'next/router';
import {Auth} from './Auth';
import {secretKey} from '../../config';
import Web3 from "web3";
import EstateOwnership from "../../build/contracts/EstateOwnership.json";

function AuthForm(props) {
    const router = useRouter();
    const [lastRoute, setLastRoute] = useState('/manage-real-estate');
    const [estateOwnershipContract, setEstateOwnershipContract] = useState(null);

    useEffect(() => {
        if (estateOwnershipContract) {
            Auth().saveAddress(jwt.sign(JSON.stringify(estateOwnershipContract), secretKey));
            router.push({
                pathname: lastRoute
            })
        }
    }, [estateOwnershipContract])

    async function loadWeb3() {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum)
            await window.ethereum.enable()
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
        }
    }

    async function loadBlockchainData() {
        const web3 = window.web3
        // Load account
        const accounts = await web3.eth.getAccounts();
        const networkId = await web3.eth.net.getId();
        const networkEstateOwnershipData = EstateOwnership.networks[networkId];

        if (networkEstateOwnershipData) {
            let estateOwnershipContract = new web3.eth.Contract(EstateOwnership.abi, networkEstateOwnershipData.address);
            setEstateOwnershipContract(estateOwnershipContract);


        } else {
            window.alert('Marketplace contract not deployed to detected network.')
        }
    }

    function onClickLogin() {
        loadWeb3().then(() => {
            loadBlockchainData();
        });
    }

    useEffect(() => {
        if (router.pathname !== '/login') {
            setLastRoute(router.pathname);
        }
    }, [router.pathname]);

    function onKeyPressForm(e) {
        if (e && e.which === 13) {
            onClickLogin();
        }
    }

    return (
        <div onKeyPress={onKeyPressForm} className="mainContainerformLogin">'
            <img src="/metamask-273x300.png" className="logoMetaMask" alt="Logo SupImmo"/>
            <Button style={{width: '100%'}} onClick={onClickLogin} type="primary">Connexion à MetaMask</Button>
        </div>
    );
}

export default AuthForm;