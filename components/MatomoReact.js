import React, {useEffect} from 'react';
import {useRouter} from 'next/router';

function MatomoReact(props) {
  const router = useRouter();

  useEffect(() => {
    /*const _paq = window._paq || [];
    let currentUrl = window.location.href;
    _paq.push(['setReferrerUrl', currentUrl]);
    currentUrl = '/' + router.pathname.substr(1);
    _paq.push(['setCustomUrl', currentUrl]);
    _paq.push(['deleteCustomVariables', 'page']);
    _paq.push(['setGenerationTimeMs', 0]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);*/
  }, []);

  return (
    <></>
  );
}

export default MatomoReact;
