import React, {useEffect, useState} from 'react';
import './style.less';
import {Tooltip} from 'antd';
import {formatInfoIfEmpty} from '../Utils';
import {Auth} from '../Auth/Auth';
import {getUserByMatricule} from '../Requests/UserRequest';
import {
  AccountBadgeFilledIcon,
  AccountFilledIcon,
  CustomIconText,
  DesktopPhoneFilledIcon,
  EmailFilledIcon,
  GenderFemaleOutlineIcon,
  GenderMaleOutlineIcon,
  PhoneFilledIcon,
  SmartphoneFilledIcon
} from '../CustomIcon';

function Account(props) {
  const [accountInfo, setAccountInfo] = useState({});

  useEffect(() => {
    const token = Auth().verifyToken();
    getUserByMatricule(token.matricule).then(response => {
      setAccountInfo(response);
    });
  }, []);

  function formatSexe(sexe) {
    let sexeFormat = '';
    let sexeIcon = '';
    if (sexe === 'M') {
      sexeFormat = 'Monsieur';
      sexeIcon = <CustomIconText size={16} placement={0} svg={GenderMaleOutlineIcon}/>;
    } else if (sexe === 'F') {
      sexeFormat = 'Madame';
      sexeIcon = <CustomIconText size={16} placement={0} svg={GenderFemaleOutlineIcon}/>;
    }

    return <span className="italic"> ({sexeFormat}{sexeIcon})</span>;
  }

  return (
    <div>
      <div className="titlePartAccount">Information agent</div>
      <div>
        <div>
          <CustomIconText svg={AccountFilledIcon}>
            <Tooltip title="Nom et Prénom">
              <span className="textIconAccount">{accountInfo.name_firstname} {formatSexe(accountInfo.sexe)}</span>
            </Tooltip>
          </CustomIconText>
        </div>
        <div>
          <CustomIconText svg={AccountBadgeFilledIcon}>
            <Tooltip title="Matricule">
              <span className="textIconAccount">{accountInfo.matricule} (Matricule)</span>
            </Tooltip>
          </CustomIconText>
        </div>
        <div>
          <CustomIconText svg={EmailFilledIcon}>
            <Tooltip title="Mail">
              <span className="textIconAccount">{formatInfoIfEmpty(accountInfo.mail)}</span>
            </Tooltip>
          </CustomIconText>
        </div>
        <div>
          <CustomIconText svg={PhoneFilledIcon}>
            <Tooltip title="Téléphone Fix">
              <span className="textIconAccount">{formatInfoIfEmpty(accountInfo.fix_phone)}</span>
            </Tooltip>
          </CustomIconText>
        </div>
        <div>
          <CustomIconText svg={SmartphoneFilledIcon}>
            <Tooltip title="Téléphone Mobile">
              <span className="textIconAccount">{formatInfoIfEmpty(accountInfo.mobile_phone)}</span>
            </Tooltip>
          </CustomIconText>
        </div>
        <div>
          <CustomIconText svg={DesktopPhoneFilledIcon}>
            <Tooltip title="Téléphone Poste">
              <span className="textIconAccount">{formatInfoIfEmpty(accountInfo.workstation_phone_number)}</span>
            </Tooltip>
          </CustomIconText>
        </div>
      </div>
      <div className="titlePartAccount">Information poste</div>
      <div>
        <div style={{marginBottom: 5}}><span className="semiBold">Pole:</span> {accountInfo.niv_pole}</div>
        <div style={{marginBottom: 5}}><span className="semiBold">Service:</span> {accountInfo.service}</div>
        <div style={{marginBottom: 5}}><span className="semiBold">Manager:</span> {accountInfo.manager}</div>
      </div>
    </div>
  );
}

export default Account;