import React, {useEffect, useState} from 'react';
import {Auth} from '../Auth/Auth';
import {Avatar, Dropdown, Menu, Modal} from 'antd';
import './style.less'
import {AccountFilledIcon, CustomIconText, DropdownOutlineIcon, LogoutOutlineIcon} from '../CustomIcon';
import {useRouter} from 'next/router';
import Account from './Account';
import Icon from '@ant-design/icons';

function ComponentHeaderProfil(props) {
    const router = useRouter();
    const [profilInfo, setProfilInfo] = useState({});
    const [visibleModal, setVisibleModal] = useState(false);
    const [selectedAddress, setSelectedAddress] = useState(false);

    const styleIcon = {
        fontSize: 18,
        position: 'relative',
        top: 2
    };

    useEffect(() => {
        const verifToken = Auth().verifyAddress();
        console.log("verifToken", verifToken);
        if (verifToken) {
            setProfilInfo(verifToken);
            setSelectedAddress(verifToken._provider.selectedAddress);
        }

        if (router.pathname === '/login') {
            setProfilInfo({});
        }
    }, [router.pathname]);

    function getLinkAvatar() {
        let avatar = null;

        return avatar;
    }

    function contentPopover() {
        return <Menu>
            <Menu.Item key="disconnect">
        <span className="nav-text" onClick={() => Auth().disconnect(router)}>
          <Icon style={styleIcon} component={LogoutOutlineIcon}/>Déconnexion
        </span>
            </Menu.Item>
        </Menu>;
    }


    return (
        <div hidden={!profilInfo._address}>
            <Dropdown overlay={contentPopover()}>
                <div className="containerPopoverProfilHeader">
                    <div>
                        <Avatar src={getLinkAvatar()} size="large"/>
                    </div>
                    <div className="containerInfosProfil">
                        <div className="addressProfil">{selectedAddress}</div>
                    </div>
                    <div className="iconDownProfilHeader">
                        <CustomIconText size={24} placement={1} svg={DropdownOutlineIcon}/>
                    </div>
                </div>
            </Dropdown>
            <Modal
                width={350}
                visible={visibleModal}
                onCancel={() => setVisibleModal(false)}
                footer={null}>
                <Account/>
            </Modal>
        </div>
    );
}

export default ComponentHeaderProfil;