import React from 'react';
import './style.less';
import ComponentHeaderProfil from '../HeaderProfil/ComponentHeaderProfil';

function HeaderCustom(props) {
  return (
    <div className="mainContainerHeader">
      <div className="containerHeaderProfil">
        <ComponentHeaderProfil />
      </div>
    </div>
  );
}

export default HeaderCustom;