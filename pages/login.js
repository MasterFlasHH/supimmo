import React from 'react';
import LayoutCustom from '../components/LayoutCustom/LayoutCustom';
import AuthForm from "../components/Auth/AuthForm";

function Login(props) {


    return (
        <LayoutCustom title="Identification" description="Page d'identification des utilisateurs">
            <AuthForm/>
        </LayoutCustom>
    );
}

export default Login;