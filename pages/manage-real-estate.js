import React, {useEffect, useState} from 'react';
import LayoutCustom from '../components/LayoutCustom/LayoutCustom';
import {Button, Col, Image, Input, Modal, Row, Select, Table} from "antd";
import {PlusCircleOutlined, ShoppingCartOutlined} from "@ant-design/icons";
import InputCheck from "../components/InputCheck/InputCheck";
import EstateFactory from "../build/contracts/EstateFactory.json";
import EstateOwnership from "../build/contracts/EstateOwnership.json";
import Web3 from "web3";
import {Auth} from "../components/Auth/Auth";

const {Option} = Select;

function ManageRealEstate(props) {
    const columns = [
        {
            title: 'Aperçu',
            dataIndex: 'overview',
            key: 'overview',
            render: (text, record) => (
                <Image
                    width={200}
                    src={text}
                />
            ),
        },
        {
            title: 'Nom',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Prix',
            dataIndex: 'price',
            key: 'price',
        },
        {
            title: 'Adresse',
            dataIndex: 'physical_address',
            key: 'physical_address',
        },
        {
            title: 'Surface',
            dataIndex: 'squared_meter_size',
            key: 'squared_meter_size',
        },
        {
            title: 'Type de bien',
            dataIndex: 'estate_type',
            key: 'estate_type',
        },
        {
            title: '',
            key: 'action',
            render: (text, record) => (
                <Button type="primary" icon={<ShoppingCartOutlined/>} onClick={() => onClickBuy(record)}/>
            ),
        },
    ];
    const [modalAddReadEstateVisibility, setModalAddReadEstateVisibility] = useState(false);
    const [overview, setOverview] = useState('');
    const [name, setName] = useState('');
    const [price, setPrice] = useState(0);
    const [address, setAddress] = useState('');
    const [size, setSize] = useState(0);
    const [type, setType] = useState(1);
    const [estateFactoryContract, setEstateFactoryContract] = useState(null);
    const [estateOwnershipContract, setEstateOwnershipContract] = useState(null);
    const [estates, setEstates] = useState([]);
    const [selectedUserAddress, setSelectedUserAddress] = useState('');
    const [tokenId, setTokenId] = useState(3);


    useEffect(() => {
        loadWeb3().then(() => {
            loadBlockchainData();
        });
    }, [])

    useEffect(() => {
        if (estateFactoryContract) {
            console.log('estateFactoryContract', estateFactoryContract);
        }
    }, [estateFactoryContract])

    async function loadWeb3() {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum)
            await window.ethereum.enable()
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
        }
    }

    async function loadBlockchainData() {
        const web3 = window.web3
        // Load account
        const accounts = await web3.eth.getAccounts();
        const networkId = await web3.eth.net.getId();
        const networkEstateFactoryData = EstateFactory.networks[networkId];
        const networkOwnershipData = EstateOwnership.networks[networkId];

        if (networkEstateFactoryData) {
            let estateFactoryContract = new web3.eth.Contract(EstateFactory.abi, networkEstateFactoryData.address);
            setEstateFactoryContract(estateFactoryContract);

            let estateOwnershipContract = new web3.eth.Contract(EstateOwnership.abi, networkOwnershipData.address);
            setEstateOwnershipContract(estateOwnershipContract);

            const verifToken = Auth().verifyAddress();
            if (verifToken) {
                setSelectedUserAddress(verifToken._provider.selectedAddress);
            }

        } else {
            window.alert('Marketplace contract not deployed to detected network.')
        }
    }

    async function getEstates(contract) {
        return await contract.methods.estates(0).call();
    }

    async function buyEstate(token) {
        await estateOwnershipContract.methods.approve(selectedUserAddress, tokenId).send({from: selectedUserAddress});
        await estateOwnershipContract.methods.takeOwnership(tokenId, token.price).send({from: selectedUserAddress});
    }

    function onOkModal() {
        createEstate({
            name: name,
            address: address,
            size: size,
            type: type,
            price: price,
            overview: overview
        }).then((response) => {
            if (response) {
                setModalAddReadEstateVisibility(false);
            }
        })
    }

    function onClickBuy(record) {
        buyEstate(record).then((response) => {
            console.log(response);
        })
    }

    function formatTypeToken(id) {
        switch (id) {
            case '1':
                return 'Appartement';
            case '2':
                return 'Maison';
            case '3':
                return 'Garage';
            case '4':
                return 'Immeuble';
            case '5':
                return 'Champs';
        }
    }

    async function getEstatesRequest() {
        getEstates(estateFactoryContract).then((response) => {
            let estatesTmp = [];
            if (response) {
                console.log('resp', response);
                [response].map((item) => {
                    estatesTmp.push({
                        id: tokenId,
                        name: item[0],
                        physical_address: item[1],
                        squared_meter_size: item[2],
                        estate_type: formatTypeToken(item[3]),
                        price: item[4],
                        overview: item[5],
                    });
                })
            }
            console.log('estatesTmp', estatesTmp);
            setEstates(estatesTmp)
        });
    }

    async function createEstate(estate) {
        await estateFactoryContract.methods.createEstate(estate.name, estate.address, estate.size, estate.type, estate.price, estate.overview).send({from: selectedUserAddress});
    }

    return (
        <>
            <LayoutCustom title="Nos annonces immobilières" description="Description Nos annonces immobilières">
                <h1 className="title">Nos annonces immobilières</h1>
                <Row gutter={[16, 16]}>
                    <Col span={3}>
                        <Button icon={<PlusCircleOutlined/>} type="primary" size={"large"}
                                onClick={() => setModalAddReadEstateVisibility(true)}>Ajouter un bien</Button>
                    </Col>
                    <Col span={3}>
                        <Button icon={<PlusCircleOutlined/>} type="primary" size={"large"}
                                onClick={() => getEstatesRequest()}>init liste</Button>
                    </Col>
                    <Table className="width100" columns={columns} dataSource={estates}/>
                </Row>
            </LayoutCustom>


            <Modal title="Ajouter un bien immobilier à la vente" visible={modalAddReadEstateVisibility}
                   onCancel={() => {
                       setModalAddReadEstateVisibility(false)
                   }} onOk={onOkModal}>
                <InputCheck label="Lien de l'aperçu">
                    <Input placeholder="Lien de l'aperçu" onChange={(e) => setOverview(e.target.value)}/>
                </InputCheck>
                <InputCheck label="Nom">
                    <Input placeholder="Nom du bien" onChange={(e) => setName(e.target.value)}/>
                </InputCheck>
                <InputCheck label="Prix">
                    <Input placeholder="Prix du bien" onChange={(e) => setPrice(e.target.value)}/>
                </InputCheck>
                <InputCheck label="Adresse">
                    <Input placeholder="Adresse du bien" onChange={(e) => setAddress(e.target.value)}/>
                </InputCheck>
                <InputCheck label="Surface">
                    <Input placeholder="Surface du bien" onChange={(e) => setSize(e.target.value)}/>
                </InputCheck>
                <InputCheck label="Type">
                    <Select className={"width100"} placeholder="Type" onChange={setType}>
                        <Option value={1}>Appartement</Option>
                        <Option value={2}>Maison</Option>
                        <Option value={3}>Garage</Option>
                        <Option value={4}>Immeuble</Option>
                        <Option value={5}>Champs</Option>
                    </Select>
                </InputCheck>
            </Modal>
        </>
    );
}

export default ManageRealEstate;
