import Document, {Head, Html, Main, NextScript} from 'next/document'
import React from 'react';

class CustomDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="fr">
        <Head/>
        <script type="text/javascript" src="/js/matomo-head.js"/>
        <body>
        <Main/>
        <NextScript/>
        </body>
      </Html>
    )
  }
}

export default CustomDocument
