import React from 'react';
import App from 'next/app';
import './_app-style.less';
import Link from 'next/link';
import MenuCustom from '../components/MenuCustom/MenuCustom';
import {ConfigProvider, Layout} from 'antd';
import {AnimatePresence} from 'framer-motion';
import FooterCustom from '../components/FooterCustom/FooterCustom';
import HeaderCustom from '../components/HeaderCustom/HeaderCustom';
import frFR from 'antd/lib/locale/fr_FR';
import moment from 'moment';
import 'moment/locale/fr';
import {RouterAccessControl} from '../components/Auth/Auth';

moment.locale('fr');

const { Content, Footer, Sider } = Layout;

class CustomApp extends App {
  render() {
    const widthSider = 220;
    const { Component, pageProps, router } = this.props;

    return (
      <ConfigProvider locale={frFR}>
        <Layout>
          <Sider theme="light" width={widthSider} style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }}>
            <div className="headerLogo">
              <Link href="/page1">
                <img src="/logo_large.png" className="logo" alt="Logo SupImmo"/>
              </Link>
            </div>
            <MenuCustom/>
          </Sider>
          <Layout style={{ marginLeft: widthSider, minHeight: '100vh' }}>
            <div style={{ position: 'fixed', zIndex: 1, height: 70, right: 0, left: widthSider, background: '#fff', padding: 0, margin: 0 }}>
              <HeaderCustom/>
            </div>
            <Content style={{ margin: '100px 30px 30px 30px', overflow: 'initial' }}>
              <RouterAccessControl router={router}>
                <AnimatePresence exitBeforeEnter>
                  <Component {...pageProps} key={router.route}/>
                </AnimatePresence>
              </RouterAccessControl>
            </Content>
            <Footer>
              <FooterCustom/>
            </Footer>
          </Layout>
        </Layout>
      </ConfigProvider>
    );
  }
}

export default CustomApp;
