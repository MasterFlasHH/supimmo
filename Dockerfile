FROM node:alpine

WORKDIR /app
COPY . .

RUN yarn install
RUN yarn build

# Copie et ajout du script d'initialisation au démarrage du container
COPY entrypoint.sh /usr/local/bin/
RUN sed -i -e 's/\r$//' /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["sh", "/usr/local/bin/entrypoint.sh"]

EXPOSE 3000
